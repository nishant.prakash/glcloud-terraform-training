resource "aws_instance" "bastion" {
  ami                         = var.ami
  instance_type               = var.instance_type
  associate_public_ip_address = var.associate_public_ip
  subnet_id                   = aws_subnet.subnet2.id
  key_name                    = var.key_name
  vpc_security_group_ids      = [aws_security_group.bastion_sg.id]
  tags = {
    Name = "Bastion_Host"
  }
}