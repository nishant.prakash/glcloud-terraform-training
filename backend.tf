terraform {
  backend "s3" {
    bucket       = "glcloud-terraform-state-bucket"
    key          = "terraform.tfstate"
    region = "us-east-1"
    profile      = "default"
  }
}
