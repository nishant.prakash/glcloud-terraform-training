resource "aws_eip" "eip" {
  vpc = true
}

resource "aws_nat_gateway" "ngw" {
  subnet_id     = aws_subnet.subnet2.id
  allocation_id = aws_eip.eip.id
}
