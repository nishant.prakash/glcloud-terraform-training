resource "aws_route_table" "route_table-pvt" {
  vpc_id = var.vpc_id
  tags   = var.tag-private
}

resource "aws_route_table_association" "rt-association-pvt" {
  route_table_id = aws_route_table.route_table-pvt.id
  subnet_id      = aws_subnet.subnet1.id
}

resource "aws_route_table_association" "rt-Association-pvt1" {
  route_table_id = aws_route_table.route_table-pvt.id
  subnet_id      = aws_subnet.subnet1-AZ2.id
}

resource "aws_route" "route" {
  route_table_id         = aws_route_table.route_table-pvt.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.ngw.id
}