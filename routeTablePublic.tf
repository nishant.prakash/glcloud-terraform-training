resource "aws_route_table" "route_table" {
  vpc_id = var.vpc_id
  tags   = var.tags
}

resource "aws_route_table_association" "rt-association" {
  route_table_id = aws_route_table.route_table.id
  subnet_id      = aws_subnet.subnet2-AZ2.id
}

resource "aws_route_table_association" "rt-Association" {
  route_table_id = aws_route_table.route_table.id
  subnet_id      = aws_subnet.subnet2.id
}

resource "aws_route" "routing-igw" {
  route_table_id         = aws_route_table.route_table.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = var.igw_id
}