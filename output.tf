output "subnet1_id" {
  value = aws_subnet.subnet1.id
}

output "subnet1_cidr" {
  value = aws_subnet.subnet1.cidr_block
}

output "subnet2_id" {
  value = aws_subnet.subnet2.id
}

output "subnet2_cidr" {
  value = aws_subnet.subnet2.cidr_block
}

output "subnet1_AZ2_id" {
  value = aws_subnet.subnet1-AZ2.id
}

output "subnet1_AZ2_cidr" {
  value = aws_subnet.subnet1-AZ2.cidr_block
}

output "subnet2_AZ2_id" {
  value = aws_subnet.subnet2-AZ2.id
}

output "subnet2_AZ2_cidr" {
  value = aws_subnet.subnet2-AZ2.cidr_block
}

