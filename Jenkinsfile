pipeline {
    agent 'any'
    stages {
        stage('Initialize Terraform') {
            steps {
                script {
                    try {
                        sh "terraform init"
                        
                    }
                    catch(Exception e) {
                        def buildStatus = 'ERROR'
                        def colorName = 'RED'
                        def colorCode = '#FF0000'
                        def subject = "ENVIRONMENT: QA \nJOB_NAME: ${env.JOB_NAME} \nBUILD_ID: ${env.BUILD_NUMBER} \nBUILD_STATUS: ${buildStatus}"
                        def summary = "Terraform build was not successful \nBUILD_URL: ${env.BUILD_URL}"
                        if (buildStatus == 'STARTED') {
                            color = 'YELLOW'
                            colorCode = '#FFFF00'
                        } else if (buildStatus == 'SUCCESSFUL') {
                            color = 'GREEN'
                            colorCode = '#00FF00'
                        } else {
                            color = 'RED'
                            colorCode = '#FF0000'
                        }
                        slackSend (channel: "test-notification", color: colorCode, message: summary)
                        throw e
                    }
                }
            }
        }

        stage('Lint terragrunt') {
            steps {
                script {
                    try {
                        sh "terraform fmt -list=true -write=false -diff=true"
                    }
                    catch(Exception e) {
                        def buildStatus = 'ERROR'
                        def colorName = 'RED'
                        def colorCode = '#FF0000'
                        def subject = "ENVIRONMENT: QA \nJOB_NAME: ${env.JOB_NAME} \nBUILD_ID: ${env.BUILD_NUMBER} \nBUILD_STATUS: ${buildStatus}"
                        def summary = "Terraform build was not successful \nBUILD_URL: ${env.BUILD_URL}"
                        if (buildStatus == 'STARTED') {
                            color = 'YELLOW'
                            colorCode = '#FFFF00'
                        } else if (buildStatus == 'SUCCESSFUL') {
                            color = 'GREEN'
                            colorCode = '#00FF00'
                        } else {
                            color = 'RED'
                            colorCode = '#FF0000'
                        }
                        slackSend (channel: "test-notification", color: colorCode, message: summary)
                        throw e
                    }
                }
            }
        }


        stage('Plan Terraform') {
            steps {
                script {
                    try {
                        sh "terraform plan"
                    }
                    catch(Exception e) {
                        def buildStatus = 'ERROR'
                        def colorName = 'RED'
                        def colorCode = '#FF0000'
                        def subject = "ENVIRONMENT: QA \nJOB_NAME: ${env.JOB_NAME} \nBUILD_ID: ${env.BUILD_NUMBER} \nBUILD_STATUS: ${buildStatus}"
                        def summary = "Terraform build was not successful \nBUILD_URL: ${env.BUILD_URL}"
                        if (buildStatus == 'STARTED') {
                            color = 'YELLOW'
                            colorCode = '#FFFF00'
                        } else if (buildStatus == 'SUCCESSFUL') {
                            color = 'GREEN'
                            colorCode = '#00FF00'
                        } else {
                            color = 'RED'
                            colorCode = '#FF0000'
                        }
                        slackSend (channel: "test-notification", color: colorCode, message: summary)
                        throw e
                    }
                }
            }
        }

        stage('Conformation to Setup instance') {
            steps {
                input 'Please check the creation of resources in previous stage and confirm configurations to setup AWS instance / instances:'
            }
        }

        stage('Execute Terraform') {
            steps {
                script {
                    try {
                        sh "terraform apply --auto-approve"
                    }
                    catch(Exception e) {
                        def buildStatus = 'ERROR'
                        def colorName = 'RED'
                        def colorCode = '#FF0000'
                        def subject = "ENVIRONMENT: QA \nJOB_NAME: ${env.JOB_NAME} \nBUILD_ID: ${env.BUILD_NUMBER} \nBUILD_STATUS: ${buildStatus}"
                        def summary = "Terraform build was not successful \nBUILD_URL: ${env.BUILD_URL}"
                        if (buildStatus == 'STARTED') {
                            color = 'YELLOW'
                            colorCode = '#FFFF00'
                        } else if (buildStatus == 'SUCCESSFUL') {
                            color = 'GREEN'
                            colorCode = '#00FF00'
                        } else {
                            color = 'RED'
                            colorCode = '#FF0000'
                        }
                        slackSend (channel: "test-notification", color: colorCode, message: summary)
                        throw e
                    }
                }
            }
        }
        
        stage('Successful Notification') {
            steps {
                script {
                    try {
                        echo "Successfully executed the pipeline"
                        def buildStatus = 'SUCCESSFUL'
                        def colorName = 'RED'
                        def colorCode = '#FF0000'
                        def subject = "ENVIRONMENT: QA \nJOB_NAME: ${env.JOB_NAME} \nBUILD_ID: ${env.BUILD_NUMBER} \nBUILD_STATUS: ${buildStatus}"
                        def summary = "Terraform build was successful \nBUILD_URL: ${env.BUILD_URL}"
                        if (buildStatus == 'STARTED') {
                            color = 'YELLOW'
                            colorCode = '#FFFF00'
                        } else if (buildStatus == 'SUCCESSFUL') {
                            color = 'GREEN'
                            colorCode = '#00FF00'
                        } else {
                            color = 'RED'
                            colorCode = '#FF0000'
                        }
                        slackSend (channel: "test-notification", color: colorCode, message: summary)
                    }
                    catch(Exception e) {
                        throw e
                    }
                }
            }
        }
    }
}
