resource "aws_security_group" "bastion_sg" {
  name   = var.bastion_sg
  vpc_id = var.vpc_id
  ingress {
    description = "Opening port 22 for bastion host"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] #Allowed for all IP ranges
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "bastion_sg"
  }
}