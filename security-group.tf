resource "aws_security_group" "alb1-sg" {
  name   = "alb-1-sg"
  vpc_id = var.vpc_id

  ingress {
    description = "Opening port 80 for a single ip"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = var.whitelisted-ip
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "alb-1-sg"
  }
}

resource "aws_security_group" "asg1-sg" {
  name   = "asg-1-sg"
  vpc_id = var.vpc_id

  ingress {
    description     = "Opening port 80 for a single ip"
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [aws_security_group.alb1-sg.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "asg1-sg"
  }
}


resource "aws_security_group" "alb2-sg" {
  name   = "alb-2-sg"
  vpc_id = var.vpc_id

  ingress {
    description     = "Opening port 80 for a single ip"
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [aws_security_group.asg1-sg.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "alb2-sg"
  }
}


resource "aws_security_group" "asg2-sg" {
  name   = "asg-2-sg"
  vpc_id = var.vpc_id

  ingress {
    description     = "Opening port 80 for a single ip"
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [aws_security_group.alb2-sg.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "asg-2-sg"
  }
}