variable "vpc_id" {
  type        = string
  description = "VPC id"
  default     = "vpc-0425bfa17c766591a"
}
variable "igw_id" {
  type        = string
  description = "IGW id"
  default     = "igw-0b554584b668048dc"
}
variable "cidr" {
  type        = string
  description = "The CIDR block for the VPC"
  default     = "10.0.0.0/16"
}
variable "instance_tenancy" {
  type        = string
  description = "A tenancy option for instances launched into the VPC"
  default     = "default"
}
variable "tag" {
  type        = map(string)
  description = "A map of tags to assign to the resource"
  default = {
    "Name" = "VPC"
  }
}
variable "dns_support" {
  type        = bool
  description = "A boolean flag to enable/disable DNS support in the VPC"
  default     = true
}
variable "dns_hostnames" {
  type        = bool
  description = "A boolean flag to enable/disable DNS hostnames in the VPC"
  default     = false
}
variable "subnet1_cidr" {
  type        = string
  description = "The CIDR block for the subnet"
  default     = "10.0.6.0/24"
}
variable "subnet2_cidr" {
  type        = string
  description = "The CIDR block for the subnet"
  default     = "10.0.7.0/24"
}
variable "subnet1_cidr-AZ2" {
  type        = string
  description = "The CIDR block for the subnet"
  default     = "10.0.8.0/24"
}
variable "subnet2_cidr-AZ2" {
  type        = string
  description = "The CIDR block for the subnet"
  default     = "10.0.9.0/24"
}
variable "az_zone1" {
  type        = string
  description = "The availability zone for the subnet"
  default     = "us-east-1a"
}
variable "az_zone2" {
  type        = string
  description = "The availability zone for the subnet"
  default     = "us-east-1b"
}
variable "map_public_ip_on_launch" {
  type        = bool
  description = "It indicates that instances launched into the subnet should be assigned a public IP address. Setting it to 'true' will create public subnet"
  default     = false
}
variable "map_ip_on_launch" {
  type        = bool
  description = "It indicates that instances launched into the subnet should be assigned a public IP address. Setting it to 'true' will create public subnet"
  default     = true
}
variable "subnet_tag" {
  type        = map(string)
  description = "A map of tags to assign to the resource"
  default = {
    "Name" = "subnet"
  }
}
variable "alb" {
  type        = string
  description = "Name of the application load balancer"
  default     = "aws-alb-1"
}
variable "internal" {
  type        = bool
  description = "Specify whether ALB is internal or not"
  default     = true
}


variable "alb-sg" {
  type        = string
  description = "Name of the security group"
  default     = "alb-1-sg"
}
variable "whitelisted-ip" {
  type        = list(string)
  description = "IP address allowed to access ALB at port 80"
  default     = ["0.0.0.0/0"]
}
variable "launch-template" {
  type        = string
  description = "Name of launch template"
  default     = "ec2-template"
}
variable "iam_instance_profile_arn" {
  type        = string
  description = "Name of instance profile"
  default     = "test"
}
variable "image_id" {
  type        = string
  description = "Image Id"
  default     = "ami-0dc2d3e4c0f9ebd18"
}
variable "instance_type" {
  type        = string
  description = "type of instance"
  default     = "t2.micro"
}
variable "key_name" {
  type        = string
  description = "key name"
  default     = "test"
}
variable "vpc_security_group_ids" {
  type        = string
  description = "Image Id"
  default     = "test"
}
variable "user_data" {
  type        = string
  description = "Image Id"
  default     = "test"
}
variable "ALB" {
  type        = string
  description = "Name of external ALB"
  default     = "aws-ALB-1"
}
variable "external" {
  type        = bool
  description = "Specify whether ALB is internal or not"
  default     = false
}
variable "tags" {
  type        = map(string)
  description = "Tags given to the Route Table"
  default = {
    "Name" = "Route-Table-public"
  }
}
variable "tag-private" {
  type        = map(string)
  description = "Tags given to the Route Table"
  default = {
    "Name" = "Route-Table-private"
  }
}
variable "salary-launch-template" {
  type        = string
  description = "Name of launch template(salary)"
  default     = "ec2-template-salary"
}
variable "key_name_Salary" {
  type        = string
  description = "key name"
  default     = "test" #This key pair must exist in AWS
}
variable "instance_type_salary" {
  type        = string
  description = "type of instance"
  default     = "t2.micro"
}
variable "image_id_salary" {
  type        = string
  description = "Image Id"
  default     = "ami-0dc2d3e4c0f9ebd18"
}
variable "sg-alb-frontend" {
  type        = string
  description = "Name of the security group"
  default     = "sg_alb-1_frontend"
}
variable "bashion-key" {
  type        = string
  description = "key name"
  default     = "test"
}
variable "ami" {
  type        = string
  description = "Bashion AMI"
  default     = "ami-0dc2d3e4c0f9ebd18"
}
variable "bastion_sg" {
  type        = string
  description = "Name of Security Group for bastion host"
  default     = "Bastion_Security_Group"
}
variable "private_instance_sg" {
  type        = string
  description = "Name of Security Group for private instances"
  default     = "Private_Instance_Security_Group"
}
variable "associate_public_ip" {
  type        = bool
  description = "Specifies whether to associate public IP or not"
  default     = true
}