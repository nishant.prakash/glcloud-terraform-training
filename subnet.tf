#Private subnet in AZ1
resource "aws_subnet" "subnet1" {
  vpc_id                  = var.vpc_id
  cidr_block              = var.subnet1_cidr
  availability_zone       = var.az_zone1
  map_public_ip_on_launch = var.map_public_ip_on_launch
  tags                    = var.subnet_tag
}

#Public subnet in AZ1
resource "aws_subnet" "subnet2" {
  vpc_id                  = var.vpc_id
  cidr_block              = var.subnet2_cidr
  availability_zone       = var.az_zone1
  map_public_ip_on_launch = var.map_ip_on_launch
  tags                    = var.subnet_tag
}

#Private subnet in AZ2
resource "aws_subnet" "subnet1-AZ2" {
  vpc_id                  = var.vpc_id
  cidr_block              = var.subnet1_cidr-AZ2
  availability_zone       = var.az_zone2
  map_public_ip_on_launch = var.map_public_ip_on_launch
  tags                    = var.subnet_tag
}

#Public subnet in AZ2
resource "aws_subnet" "subnet2-AZ2" {
  vpc_id                  = var.vpc_id
  cidr_block              = var.subnet2_cidr-AZ2
  availability_zone       = var.az_zone2
  map_public_ip_on_launch = var.map_ip_on_launch
  tags                    = var.subnet_tag
}